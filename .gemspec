Gem::Specification.new do |spec|
  spec.name          = "jekyll-theme-cecs"
  spec.version       = "2.0.0.beta"
  spec.authors       = ["Ben Swift", "Harrison Shoebridge"]
  spec.email         = ["helpdesk@cecs.anu.edu.au"]

  spec.summary       = "ANU CECS Theme for Jekyll Pages with gitlab"
  spec.homepage      = ""
  spec.license       = "Nonstandard"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r{^(assets|_layouts|_includes|_sass|css|js|LICENSE|README|feed.xml|search.json)}i) }

  spec.add_runtime_dependency "jekyll"
end
